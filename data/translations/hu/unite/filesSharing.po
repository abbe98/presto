# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: Opera Unite File sharing\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-08-31 11:17-02:00\n"
"PO-Revision-Date: 2009-10-22 13:42+0100\n"
"Last-Translator: Csaba Szökőcs <szokocs@gmail.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Language: Hungarian\n"
"X-Poedit-Country: Hungary\n"

#. Column header for the file name
#: templates/fileSharing.html
msgid "Name"
msgstr "Név"

#. Column header for the file size
#: templates/fileSharing.html
msgid "Size"
msgstr "Méret"

#. Column header for the date and time of the file
#: templates/fileSharing.html
msgid "Time"
msgstr "Időpont"

#. Link to save the chosen file
#. Shown immediately to the left of the Size column, slightly gray until hovered
#: templates/fileSharing.html
msgid "Download"
msgstr "Letöltés"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 folder"
msgstr "1 mappa"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} folders"
msgstr "{counter} mappa"

#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "and"
msgstr "és "

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 file"
msgstr "1 fájl"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} files"
msgstr "{counter} fájl"

#. Displays instead of the list of files when a folder is empty.
#: templates/fileSharing.html
msgid "This folder is empty."
msgstr "Ez a mappa üres."

#. Error page title text when a resource is not found
#: templates/fileSharing.html
msgid "Folder or file not found"
msgstr "A mappa vagy a fájl nem található"

#. A link that takes the user to the parent folder of a folder.
#. Eg.
#. '<- Parent folder'
#. '      List of files...
#: templates/fileSharing.html
msgid "Parent folder"
msgstr "Szülőmappa"
